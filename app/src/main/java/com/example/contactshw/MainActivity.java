package com.example.contactshw;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        final List<Contact> contactList = Generator.generate();

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        MyAdapter adapter = new MyAdapter(contactList, this);
        recyclerView.setAdapter(adapter);

        adapter.setListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onClick(String userName) {
//                Intent intent = new Intent(Contact.class, );
//                intent.putExtra(NAME, contactList.get())
            }
        });
    }
}
