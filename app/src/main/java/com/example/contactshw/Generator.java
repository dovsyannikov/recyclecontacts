package com.example.contactshw;

import android.media.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Contact> generate (){
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Gregory V. Gleason", "GregoryVGleason@rhyta.com", "260-482-9114", "2229 Windy Ridge Road\n" +
                "Fort Wayne, IN 46805"));
        contacts.add(new Contact("Alice S. Leclair", "AliceSLeclair@rhyta.com", "(03) 5373 0233", "5 Edmundsons Road\n" +
                "MOLLONGGHIP VIC 3352"));
        contacts.add(new Contact("Richard S. Risinger", "RichardSRisinger@teleworm.us", "(03) 8631 1819", "27 Creedon Street\n" +
                "PARKVILLE VIC 3052"));
        return contacts;

    }
}
